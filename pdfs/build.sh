cd ..
FILES_WITHOUT_EXT=$( ls -1 *.md | sed -e 's/\..*$//')

GEOMETRY="---\ngeometry: margin=2cm\nauthor: CSS builder\n---"

if ! which pandoc > /dev/null; then
	echo "please install 'pandoc'"
	exit 1
fi
if ! which pdflatex > /dev/null; then
	echo "please install latex (texlive-most for example)"
	exit 1
fi

for file in $FILES_WITHOUT_EXT
do
	echo "building '$file'..."
	INPUT_FILE="$file.md"
	TMP_FILE="tmp_builder.md"
	OUTPUT_FILE="pdfs/$file.pdf"

	echo -e $GEOMETRY > $TMP_FILE
	cat  $INPUT_FILE >> $TMP_FILE
	pandoc $TMP_FILE -o $OUTPUT_FILE
	rm $TMP_FILE
done
